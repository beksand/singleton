package pl.sda.designpatterns.singleton.zad2;

public class Game {
    public int generationLiczba1(){

        return (int) (MySettings.INSTANCE.getZakresLiczby1()*Math.random());
    }
    public int generationLiczba2(){

        return (int) (MySettings.INSTANCE.getZakresLiczby2()*Math.random());
    }
    public char generationDzalanie(){
        int index = (int) (Math.random()*MySettings.INSTANCE.getDzialanie().length());
        char dzialanie = MySettings.INSTANCE.getDzialanie().charAt(index);
        return dzialanie;
    }
    public int counter(boolean flag){
        int counter = 0;
        if (flag) {
            return counter++;
        }
        return counter;
    }
    public void printLineDzialanie(int a, int b, char dzialanie){


        switch (dzialanie) {
            case '+':
                System.out.println(a+"+"+b);
                break;
            case '-':
                System.out.println(a+"-"+b);
                break;
            case '/':
                System.out.println(a+"/"+b);
                break;
            case '*':
                System.out.println(a+"*"+b);
                break;
            default:
                System.out.println("nima dzilania");
        }
    }
    public int zrobDzialanie(int a, int b, char dzialanie){
        switch (dzialanie){
            case '+':

                 return a+b;

            case '-':

                return a-b;
            case '/':

                return a/b;
            case '*':

                return a*b;
        }
        return Integer.parseInt(null);
    }
    public boolean sprawdzResult(int result, int resultInput){
        if (result==resultInput){
            return true;
        }
        return false;
    }
}
