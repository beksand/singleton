package pl.sda.designpatterns.singleton.zad2;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
//        int r = (int) (Math.random()*100);
//        System.out.println(r);

        Game game = new Game();
        ReadConfiguration readConfiguration = new ReadConfiguration();
        readConfiguration.settings();
        Scanner sc = new Scanner(System.in);
        int counter=0;
        int round=0;
        while (round<MySettings.INSTANCE.getIloscRund()){
            round++;
            int a = game.generationLiczba1();
            int b = game.generationLiczba2();
            char dzialanie = game.generationDzalanie();
            game.printLineDzialanie(a, b, dzialanie);

//            game.sprawdzResult(game.zrobDzialanie(a, b, dzialanie), sc.nextInt());
//            counter = game.counter(game.sprawdzResult(game.zrobDzialanie(a, b, dzialanie), sc.nextInt()));
            if (game.sprawdzResult(game.zrobDzialanie(a, b, dzialanie), sc.nextInt())) {
            counter++;
            }
        }
        System.out.println("dobrze liczisz: " + counter + " razy!");
    }


}
