package pl.sda.designpatterns.singleton.zad2;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class MySettings {
    public static final MySettings INSTANCE = new MySettings();

    private MySettings(){
    }

    private int zakresLiczby1=10;
    private int zakresLiczby2=10;
    private int iloscRund=5;
    private String dzialanie="+-/*";

    public String getDzialanie() {
        return dzialanie;
    }

    public void setDzialanie(String dzialanie) {
        this.dzialanie = dzialanie;
    }

    public int getIloscRund() {
        return iloscRund;
    }

    public void setIloscRund(int iloscRund) {
        this.iloscRund = iloscRund;
    }

    public int getZakresLiczby2() {
        return zakresLiczby2;
    }

    public void setZakresLiczby2(int zakresLiczby2) {
        this.zakresLiczby2 = zakresLiczby2;
    }

    public int getZakresLiczby1() {
        return zakresLiczby1;
    }

    public void setZakresLiczby1(int zakresLiczby1) {
        this.zakresLiczby1 = zakresLiczby1;
    }
}
