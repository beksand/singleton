package pl.sda.designpatterns.singleton.zad2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadConfiguration {
    private File file = new File("settings.txt");
    private List<String> config = new ArrayList<>();

    public List<String> settings() throws FileNotFoundException {
        Scanner scfile = new Scanner(file);
        while (scfile.hasNextLine()){
            String lineSettings = scfile.nextLine();
            config.add(lineSettings);
        }
        scfile.close();
        setZakres1();
        setZakres2();
        setIloscRund();
        setDzialanie();
        return config;
    }

    public void setZakres1(){
        for (String linia : config) {
            if(linia.startsWith("zakres_liczby_1")){
                int wartosc = Integer.parseInt(linia.split("=")[1]);
                MySettings.INSTANCE.setZakresLiczby1(wartosc);
            }
        }
    }
    public void setZakres2(){
        for (String linia : config) {
            if(linia.startsWith("zakres_liczby_2")){
                int wartosc = Integer.parseInt(linia.split("=")[1]);
                MySettings.INSTANCE.setZakresLiczby2(wartosc);
            }
        }
    }
    public void setIloscRund(){
        for (String linia : config) {
            if(linia.startsWith("ilosc_rund")){
                int wartosc = Integer.parseInt(linia.split("=")[1]);
                MySettings.INSTANCE.setIloscRund(wartosc);
            }
        }
    }
    public void setDzialanie(){
        for (String linia: config) {
            if (linia.startsWith("dostepne_dzialania")){
                MySettings.INSTANCE.setDzialanie(linia.split("=")[1]);
            }
        }
    }
}
