package pl.sda.designpatterns.singleton.zad1;

public class TicketGenerator {

    public static final TicketGenerator INSTANCE = new TicketGenerator();
    private TicketGenerator(){}

    private int counter=0;
    public int getCounter(){
        return counter++;
    }
}
